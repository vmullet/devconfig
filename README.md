# DEVCONFIG

This project will store all my developement environment configs.

## Curent Configs stored

+ zsh
  + robburussell
  + powerlevel
+ powershell
  + dracula
+ Windows.Terminal
  + profiles

### zsh

.zshrc is located in ~/

### powershell

Settings location :
C:\Users\Valentin\Documents\WindowsPowershell\Microsoft.Powershell_profile.ps1

### Windows.Terminal

profiles.json location :
C:\Users\Valentin\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState
